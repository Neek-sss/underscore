#![warn(
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    trivial_numeric_casts,
    trivial_casts,
    unsafe_code,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications
)]
#![cfg_attr(feature = "cargo-clippy", warn(clippy_pedantic))]

#[macro_use]
extern crate serde_derive;
extern crate rhai;

mod graphics;
mod mods;

pub use graphics::model;
pub use graphics::traits::Vertex;
pub use graphics::Camera;
pub use graphics::InputTable;
pub use mods::engine_ext::EngineExt;
pub use mods::mod_list::ModList;
pub use mods::mod_pack::ModPack;
pub use mods::mod_pack::ModType;
