extern crate gfx;

use super::mod_pack::{ModData, ModPack, ModType};
use graphics;
use rhai::Engine;
use std::{collections::HashMap, fs::read_dir, iter::FromIterator, path::Path};

#[derive(Clone, Debug)]
pub struct ModList {
    // pub icons: Option<Vec<String>>,
    pub textures: Option<Vec<String>>,
    pub models: Option<Vec<String>>,
    pub unit_armies: Option<HashMap<String, ModData>>,
    pub unit_types: Option<HashMap<String, ModData>>,
    pub unit_classes: Option<HashMap<String, ModData>>,
    pub unit_abilities: Option<HashMap<String, ModData>>,
    pub unit_equipment: Option<HashMap<String, ModData>>,
    pub unit_interactions: Option<HashMap<String, ModData>>,
}

impl ModList {
    pub fn new() -> Self {
        Self {
            // icons: None,
            textures: None,
            models: None,
            unit_armies: None,
            unit_types: None,
            unit_classes: None,
            unit_abilities: None,
            unit_equipment: None,
            unit_interactions: None,
        }
    }

    pub fn load_mods<F, R, C, V>(
        &mut self,
        engine: &mut Engine,
        factory: &mut F,
        encoder: &mut gfx::Encoder<R, C>,
        textures: &mut HashMap<String, gfx::handle::ShaderResourceView<R, [f32; 4]>>,
        objects: &mut HashMap<String, Vec<(String, gfx::handle::Buffer<R, V>, gfx::Slice<R>)>>,
    ) where
        F: gfx::Factory<R>,
        R: gfx::Resources,
        C: gfx::CommandBuffer<R>,
        V: graphics::traits::Vertex
            + gfx::traits::Pod
            + gfx::pso::buffer::Structure<gfx::format::Format>,
    {
        for mod_pack in read_dir("./mods/").expect("Read Directory Error; Path: ./mods/") {
            let mut mod_path = mod_pack.expect("File Name Error").path();
            let loaded_mod = ModPack::load(&mod_path);
            for &(ref sub_mod_path, ref sub_mod) in &loaded_mod.load_sub_mods(&mod_path) {
                sub_mod.load_mod_sources(engine, Path::new(sub_mod_path));
                // sub_mod.load_mod_icons(factory, Path::new(sub_mod_path));
                if let Some((mut mod_textures, mut mod_objects)) =
                    sub_mod.load_mod_models::<_, _, _, V>(factory, encoder, Path::new(sub_mod_path))
                {
                    mod_objects.drain().for_each(|(key, value)| {
                        objects.insert(key, value);
                    });
                    mod_textures.drain().for_each(|(key, value)| {
                        textures.insert(key, value);
                    });
                }
                self.push(sub_mod);
            }
            loaded_mod.load_mod_sources(engine, &mod_path);
            if let Some((mut mod_textures, mut mod_objects)) =
                loaded_mod.load_mod_models::<_, _, _, V>(factory, encoder, Path::new(&mod_path))
            {
                mod_objects.drain().for_each(|(key, value)| {
                    objects.insert(key, value);
                });
                mod_textures.drain().for_each(|(key, value)| {
                    textures.insert(key, value);
                });
            }
            self.push(&loaded_mod);
        }
    }

    fn push_data<A>(first: &mut Option<A>, second: &Option<A>)
    where
        A: Clone + IntoIterator + FromIterator<<A as IntoIterator>::Item>,
    {
        *first = if let &Some(ref pack_mod_list) = second {
            if let &mut Some(ref self_mod_list) = first {
                Some(
                    self_mod_list
                        .clone()
                        .into_iter()
                        .chain(pack_mod_list.clone().into_iter())
                        .collect(),
                )
            } else {
                Some(pack_mod_list.clone())
            }
        } else {
            first.clone()
        };
    }

    pub fn push(&mut self, mod_pack: &ModPack) {
        // Self::push_data(&mut self.icons, &mod_pack.data.icons);
        // Self::push_data(&mut self.textures, &mod_pack.data.textures);
        // Self::push_data(&mut self.models, &mod_pack.data.models);
        Self::push_data(&mut self.unit_armies, &mod_pack.data.unit_armies);
        Self::push_data(&mut self.unit_types, &mod_pack.data.unit_types);
        Self::push_data(&mut self.unit_classes, &mod_pack.data.unit_classes);
        Self::push_data(&mut self.unit_abilities, &mod_pack.data.unit_abilities);
        Self::push_data(&mut self.unit_equipment, &mod_pack.data.unit_equipment);
        Self::push_data(
            &mut self.unit_interactions,
            &mod_pack.data.unit_interactions,
        );
    }

    pub fn update_table<'a>(
        &self,
        engine: &mut Engine,
        attribute_table: HashMap<String, f64>,
        mod_type: ModType,
        mods: &mut [(&str, u64)],
    ) -> HashMap<String, f64> {
        let mut temp_attribute_table = attribute_table;
        let mod_section = match mod_type {
            ModType::UnitArmy => &self.unit_armies,
            ModType::UnitType => &self.unit_types,
            ModType::UnitClass => &self.unit_classes,
            ModType::UnitAbility => &self.unit_abilities,
            ModType::UnitEquipment => &self.unit_equipment,
        };
        if let &Some(ref mod_section_names) = mod_section {
            for used_mod in mods {
                if let Some(mod_data) = mod_section_names.get(&String::from(used_mod.0)) {
                    temp_attribute_table = engine
                        .call_fn(
                            mod_data.attribute_function.clone(),
                            (&mut temp_attribute_table, &mut (used_mod.1 as f64)),
                        ).expect("Function Call Error");
                }
            }
        }
        temp_attribute_table
    }

    pub fn interact<'a>(
        &self,
        engine: &mut Engine,
        attribute_table1: HashMap<String, f64>,
        attribute_table2: HashMap<String, f64>,
        interaction: &str,
        level: u64,
    ) -> HashMap<String, f64> {
        let mut temp_attribute_table = HashMap::new();
        if let Some(ref interactions) = self.unit_interactions {
            if let Some(mod_data) = interactions.get(interaction) {
                temp_attribute_table = engine
                    .call_fn(
                        mod_data.attribute_function.clone(),
                        (
                            &mut attribute_table1.clone(),
                            &mut attribute_table2.clone(),
                            &mut (level as f64),
                        ),
                    ).expect("Function Call Error");
            }
        }
        temp_attribute_table
    }
}
