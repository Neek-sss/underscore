use rhai::{Engine, RegisterFn};
use std::collections::HashMap;

pub trait EngineExt {
    fn load_attribute_table(&mut self);
}

impl EngineExt for Engine {
    fn load_attribute_table(&mut self) {
        self.register_type::<HashMap<String, f64>>();

        self.register_fn("insert", HashMap::<String, f64>::insert);

        fn attribute_table_get_or(table: HashMap<String, f64>, name: String, default: f64) -> f64 {
            table
                .get(&name)
                .and_then(|value| Some(*value))
                .unwrap_or(default)
        };
        self.register_fn("get_or", attribute_table_get_or);

        fn attribute_table_get(table: HashMap<String, f64>, name: String) -> f64 {
            *table.get(&name).unwrap()
        };
        self.register_fn("get", attribute_table_get);

        fn attribute_table_contains_key(table: HashMap<String, f64>, name: String) -> bool {
            table.contains_key(&name)
        };
        self.register_fn("contains_key", attribute_table_contains_key);

        fn attribute_table_delete(
            table: HashMap<String, f64>,
            name: String,
        ) -> HashMap<String, f64> {
            let mut temp_table = table;
            temp_table.remove(&name);
            temp_table
        }
        self.register_fn("delete", attribute_table_delete);
    }
}
