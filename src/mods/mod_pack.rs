extern crate gfx;
extern crate toml;

use graphics;
use graphics::{model, texture};
use rhai::Engine;
use std::{
    collections::HashMap,
    fs::File,
    hash::Hash,
    io::{prelude::*, BufReader},
    path::Path,
};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FileSource {
    pub filename: Option<String>,
    pub source: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModData {
    pub name: String,
    pub description: String,
    pub attribute_function: String,
    pub model: Option<String>,
    pub icon: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TypeData {
    pub name: String,
    pub description: String,
    pub model_name: String,
    pub attribute_function: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModPackData {
    pub dependencies: Option<HashMap<String, String>>,
    pub sub_mods: Option<Vec<String>>,
    pub data_files: Option<Vec<String>>,
    pub sources: Option<Vec<FileSource>>,
    // pub icons: Option<Vec<FileSource>>,
    pub models: Option<Vec<FileSource>>,
    pub unit_armies: Option<HashMap<String, ModData>>,
    pub unit_types: Option<HashMap<String, ModData>>,
    pub unit_classes: Option<HashMap<String, ModData>>,
    pub unit_abilities: Option<HashMap<String, ModData>>,
    pub unit_equipment: Option<HashMap<String, ModData>>,
    pub unit_interactions: Option<HashMap<String, ModData>>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModPackMetaData {
    pub name: String,
    pub version: String,
    pub authors: Vec<String>,
    pub description: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModPack {
    pub meta_data: ModPackMetaData,
    pub data: ModPackData,
}

#[derive(Clone, Copy, Debug)]
pub enum ModType {
    UnitArmy,
    UnitType,
    UnitClass,
    UnitAbility,
    UnitEquipment,
}

impl ModPack {
    pub fn load(mod_path: &Path) -> Self {
        let mod_toml = mod_path.join("mod.toml");
        let unit_file = File::open(mod_toml.clone()).expect(&format!(
            "File Open Error; Path: {}",
            mod_toml.to_str().expect("Invalid Path Object")
        ));
        let mut unit_reader = BufReader::new(unit_file);
        let mut string = String::new();
        unit_reader
            .read_to_string(&mut string)
            .expect("Error Reading File");
        let mut loaded_mod: Self = toml::from_str(&string).expect("Error Parsing File To TOML");

        let mod_data = if let Some(ref data_files) = loaded_mod.data.data_files {
            data_files
                .iter()
                .map(|data_file_name| {
                    let mut data_file_toml = mod_path.join(data_file_name);
                    let data_file = File::open(data_file_toml.clone()).expect(&format!(
                        "File Open Error; Path: {}",
                        data_file_toml.to_str().expect("Invalid Path Object")
                    ));
                    let mut data_reader = BufReader::new(data_file);
                    let mut string = String::new();
                    data_reader
                        .read_to_string(&mut string)
                        .expect("Error Reading File");
                    let loaded_data: Self =
                        toml::from_str(&string).expect("Error Parsing File to TOML");
                    loaded_data
                }).collect()
        } else {
            Vec::new()
        };

        for data in &mod_data {
            loaded_mod.merge(data);
        }

        loaded_mod
    }

    fn merge_hashmap_data<A1, A2>(
        first: &mut Option<HashMap<A1, A2>>,
        second: &Option<HashMap<A1, A2>>,
    ) where
        A1: Clone + Eq + Hash,
        A2: Clone,
    {
        *first = if let &Some(ref pack_mod_list) = second {
            if let &mut Some(ref self_mod_list) = first {
                Some(
                    self_mod_list
                        .iter()
                        .chain(pack_mod_list.iter())
                        .map(|(iter_data0, iter_data1)| (iter_data0.clone(), iter_data1.clone()))
                        .collect(),
                )
            } else {
                Some(pack_mod_list.clone())
            }
        } else {
            first.clone()
        };
    }

    fn merge_vec_data<A>(first: &mut Option<Vec<A>>, second: &Option<Vec<A>>)
    where
        A: Clone,
    {
        *first = if let Some(ref other_list) = second {
            if let Some(ref self_list) = first {
                Some(
                    self_list
                        .iter()
                        .chain(other_list.iter())
                        .map(|source| source.clone())
                        .collect(),
                )
            } else {
                Some(other_list.clone())
            }
        } else {
            first.clone()
        };
    }

    pub fn merge(&mut self, other: &Self) {
        Self::merge_vec_data(&mut self.data.sources, &other.data.sources);
        Self::merge_vec_data(&mut self.data.models, &other.data.models);
        // Self::merge_vec_data(&mut self.data.icons, &other.data.icons);

        Self::merge_hashmap_data(&mut self.data.unit_armies, &other.data.unit_armies);
        Self::merge_hashmap_data(&mut self.data.unit_types, &other.data.unit_types);
        Self::merge_hashmap_data(&mut self.data.unit_classes, &other.data.unit_classes);
        Self::merge_hashmap_data(&mut self.data.unit_abilities, &other.data.unit_abilities);
        Self::merge_hashmap_data(&mut self.data.unit_equipment, &other.data.unit_equipment);
        Self::merge_hashmap_data(
            &mut self.data.unit_interactions,
            &other.data.unit_interactions,
        );
    }

    pub fn load_sub_mods(&self, mod_path: &Path) -> Vec<(String, ModPack)> {
        if let Some(ref sub_mods) = self.data.sub_mods {
            let mut loaded_sub_mods = Vec::new();
            for sub_mod in sub_mods {
                let mut sub_mod_path = mod_path.join(sub_mod);
                let loaded_sub_mod = ModPack::load(&sub_mod_path);
                let mut loaded_sub_sub_mods = loaded_sub_mod.load_sub_mods(&sub_mod_path);
                loaded_sub_mods.push((sub_mod_path.to_str().unwrap().to_string(), loaded_sub_mod));
                loaded_sub_mods.append(&mut loaded_sub_sub_mods);
            }
            loaded_sub_mods
        } else {
            Vec::new()
        }
    }

    pub fn load_mod_sources(&self, engine: &mut Engine, mod_path: &Path) {
        if let Some(ref sources) = self.data.sources {
            for source in sources.iter() {
                if let Some(ref source_str) = source.source {
                    engine
                        .eval::<()>(&source_str)
                        .expect("Error Evaluating RHAI Source");
                } else if let Some(ref filename) = source.filename {
                    let mut mod_file = mod_path.join(filename);
                    let mod_file = File::open(mod_file.clone()).expect(&format!(
                        "File Open Error; Path: {}",
                        mod_file.to_str().expect("Invalid Path Object")
                    ));
                    let mut mod_reader = BufReader::new(mod_file);
                    let mut source = String::new();
                    mod_reader
                        .read_to_string(&mut source)
                        .expect("Error Reading File");
                    engine
                        .eval::<()>(&source)
                        .expect("Error Evaluating RHAI Source");
                }
            }
        }
    }

    // pub fn load_mod_icons<F, R>(&self, factory: &mut F, mod_path: &Path)
    // where
    //     F: gfx::Factory<R>,
    //     R: gfx::Resources,
    // {
    //     if let Some(ref icons) = self.data.icons {
    //         for icon in icons.iter() {
    //             if let Some(ref icon_file_name) = icon.filename {
    //                 let icon =
    //                     texture::load_texture::<F, R>(factory, &mod_path.join(icon_file_name));
    //             }
    //         }
    //     }
    // }

    pub fn load_mod_models<F, R, C, V>(
        &self,
        factory: &mut F,
        encoder: &mut gfx::Encoder<R, C>,
        mod_path: &Path,
    ) -> Option<(
        HashMap<String, gfx::handle::ShaderResourceView<R, [f32; 4]>>,
        HashMap<String, Vec<(String, gfx::handle::Buffer<R, V>, gfx::Slice<R>)>>,
    )>
    where
        F: gfx::Factory<R> + gfx::traits::FactoryExt<R>,
        R: gfx::Resources,
        C: gfx::CommandBuffer<R>,
        V: graphics::traits::Vertex
            + gfx::traits::Pod
            + gfx::pso::buffer::Structure<gfx::format::Format>,
    {
        self.data.models.as_ref().and_then(|models| {
            Some(
                models
                    .iter()
                    .fold((HashMap::new(), HashMap::new()), |acc, model| {
                        let mut acc = acc;
                        if let Some(ref model_file_string) = model.filename {
                            let model_file_path = Path::new(model_file_string);
                            let (mut textures, mut models) = model::load_model::<_, _, _, V>(
                                model_file_path
                                    .file_name()
                                    .expect("Invalid Path Object")
                                    .to_str()
                                    .expect("Invalid Path Object"),
                                &mod_path
                                    .join(model_file_path.parent().expect("Invalid Path Object")),
                                factory,
                                encoder,
                            );
                            textures.drain().for_each(|(key, value)| {
                                acc.0.insert(key, value);
                            });
                            models.drain().for_each(|(key, value)| {
                                acc.1.insert(key, value);
                            });
                        }
                        acc
                    }),
            )
        })
    }
}
