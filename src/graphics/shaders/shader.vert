#version 150 core

in vec3 a_Pos;
in vec2 a_Uv;
in vec3 a_Norm;

uniform Uniforms {
    mat4 u_Transform;
};

out vec2 v_Uv;

void main() {
    v_Uv = a_Uv;
    gl_Position = u_Transform * vec4(a_Pos, 1.0);
}
