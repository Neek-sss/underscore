extern crate gfx;
extern crate image;

use self::gfx::format::Rgba8;
use std::path::Path;

pub fn load_texture<F, R>(
    factory: &mut F,
    file: &Path,
) -> gfx::handle::ShaderResourceView<R, [f32; 4]>
where
    F: gfx::Factory<R>,
    R: gfx::Resources,
{
    let img = image::open(file)
        .expect(
            &format!(
                "Error Opening Image; Path: {}",
                file.to_str().expect("Invalid Path Object")
            ).as_str(),
        ).to_rgba();
    let (width, height) = img.dimensions();
    let kind = gfx::texture::Kind::D2(width as u16, height as u16, gfx::texture::AaMode::Single);
    let (_, view) = factory
        .create_texture_immutable_u8::<Rgba8>(kind, gfx::texture::Mipmap::Allocated, &[&img])
        .expect("Error Creating Texture");
    view
}
