extern crate nalgebra;

use self::nalgebra::{Isometry3, Point3, Translation3, Unit, UnitQuaternion, Vector3};
use std::f32;

enum CameraType {
    LookAt {
        target: Point3<f32>,
        reset_target: Point3<f32>,
    },
    FirstPerson {
        rotation: Vector3<f32>,
        reset_rotation: Vector3<f32>,
    },
    Orbit,
}

pub struct Camera {
    position: Point3<f32>,
    reset_position: Point3<f32>,
    camera_type: CameraType,
    fov: f32,
    reset_fov: f32,
}

impl Camera {
    pub fn new() -> Self {
        Self {
            position: Point3::new(0.0, 0.0, 0.0),
            reset_position: Point3::new(0.0, 0.0, 0.0),
            camera_type: CameraType::LookAt {
                target: Point3::new(0.0, 0.0, 0.0),
                reset_target: Point3::new(0.0, 0.0, 0.0),
            },
            fov: f32::consts::FRAC_PI_2,
            reset_fov: f32::consts::FRAC_PI_2,
        }
    }

    pub fn with_position(mut self, start_position: Point3<f32>) -> Self {
        self.position = start_position;
        self.reset_position = start_position;
        self
    }

    pub fn with_target(mut self, start_target: Point3<f32>) -> Self {
        self.camera_type = CameraType::LookAt {
            target: start_target,
            reset_target: start_target,
        };
        self
    }

    pub fn with_angle(mut self, start_rotation: Vector3<f32>) -> Self {
        self.camera_type = CameraType::FirstPerson {
            rotation: start_rotation,
            reset_rotation: start_rotation,
        };
        self
    }

    pub fn move_position(&mut self, movement: Vector3<f32>) {
        self.position += movement;
    }

    pub fn move_angle(&mut self, movement: Vector3<f32>) -> bool {
        if let CameraType::FirstPerson {
            ref mut rotation, ..
        } = self.camera_type
        {
            let pitch = match rotation[0] + movement[0] {
                p if (p > f32::consts::PI) => -f32::consts::PI,
                p if (p < -f32::consts::PI) => f32::consts::PI,
                p => p,
            };
            let yaw = match rotation[1] + movement[1] {
                y if (y > f32::consts::PI) => -f32::consts::PI,
                y if (y < -f32::consts::PI) => f32::consts::PI,
                y => y,
            };
            let roll = match rotation[2] + movement[2] {
                // r if (r > f32::consts::PI) => f32::consts::PI,
                // r if (r < -f32::consts::PI) => -f32::consts::PI,
                r => r,
            };
            *rotation = Vector3::new(pitch, yaw, roll);
            true
        } else {
            false
        }
    }

    pub fn move_fov(&mut self, fov_change: f32) {
        self.fov = match self.fov + fov_change {
            // f if (f > 45.0) => 45.0,
            // f if (f < 1.0) => 1.0,
            f => f,
        };
    }

    pub fn set_position(&mut self, new_position: Point3<f32>) {
        self.position = new_position;
    }

    pub fn set_target(&mut self, new_target: Point3<f32>) -> bool {
        if let CameraType::LookAt { ref mut target, .. } = self.camera_type {
            *target = new_target;
            true
        } else {
            false
        }
    }

    pub fn set_angle(mut self, new_rotation: Vector3<f32>) -> bool {
        if let CameraType::FirstPerson {
            ref mut rotation, ..
        } = self.camera_type
        {
            *rotation = new_rotation;
            true
        } else {
            false
        }
    }

    pub fn get_position(&self) -> Point3<f32> {
        self.position
    }

    pub fn get_target(&self) -> Option<Point3<f32>> {
        if let CameraType::LookAt { target, .. } = self.camera_type {
            Some(target)
        } else {
            None
        }
    }

    pub fn get_angle(&self) -> Option<Vector3<f32>> {
        if let CameraType::FirstPerson { rotation, .. } = self.camera_type {
            Some(rotation)
        } else {
            None
        }
    }

    pub fn get_fov(&self) -> f32 {
        self.fov
    }

    pub fn reset(&mut self) {
        self.position = self.reset_position;
        match self.camera_type {
            CameraType::LookAt {
                ref mut target,
                reset_target,
            } => *target = reset_target,
            CameraType::FirstPerson {
                ref mut rotation,
                reset_rotation,
            } => *rotation = reset_rotation,
            _ => {}
        }
        self.fov = self.reset_fov;
    }

    pub fn new_reset(&mut self) {
        self.reset_position = self.position;
        match self.camera_type {
            CameraType::LookAt {
                target,
                ref mut reset_target,
            } => *reset_target = target,
            CameraType::FirstPerson {
                rotation,
                ref mut reset_rotation,
            } => *reset_rotation = rotation,
            _ => {}
        }
        self.reset_fov = self.fov;
    }

    pub fn view(&self) -> Isometry3<f32> {
        match self.camera_type {
            CameraType::LookAt { target, .. } => {
                Isometry3::look_at_rh(&self.position, &target, &Vector3::y_axis())
            }
            CameraType::FirstPerson { rotation, .. } => {
                let pitch_transform =
                    UnitQuaternion::from_axis_angle(&Vector3::x_axis(), rotation[0]);
                let yaw_transform =
                    UnitQuaternion::from_axis_angle(&Vector3::y_axis(), rotation[1]);
                let roll_transform =
                    UnitQuaternion::from_axis_angle(&Vector3::z_axis(), rotation[2]);
                roll_transform
                    * pitch_transform
                    * yaw_transform
                    * Translation3::new(self.position[0], self.position[1], self.position[2])
            }
            _ => Isometry3::identity(),
        }
    }

    pub fn axis(&self) -> (Unit<Vector3<f32>>, Unit<Vector3<f32>>, Unit<Vector3<f32>>) {
        match self.camera_type {
            CameraType::LookAt { .. } => (Vector3::x_axis(), Vector3::y_axis(), Vector3::z_axis()),
            CameraType::FirstPerson { .. } => {
                let view_matrix = self.view().to_homogeneous();
                let right = Unit::new_normalize(Vector3::new(
                    view_matrix.row(0)[0],
                    view_matrix.row(0)[1],
                    view_matrix.row(0)[2],
                ));
                let up = Unit::new_normalize(Vector3::new(
                    view_matrix.row(1)[0],
                    view_matrix.row(1)[1],
                    view_matrix.row(1)[2],
                ));
                let forward = Unit::new_normalize(Vector3::new(
                    view_matrix.row(2)[0],
                    view_matrix.row(2)[1],
                    view_matrix.row(2)[2],
                ));
                (right, up, forward)
            }
            _ => (
                Unit::new_normalize(Vector3::new(0.0, 0.0, 0.0)),
                Unit::new_normalize(Vector3::new(0.0, 0.0, 0.0)),
                Unit::new_normalize(Vector3::new(0.0, 0.0, 0.0)),
            ),
        }
    }
}
