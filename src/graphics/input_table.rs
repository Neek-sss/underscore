extern crate glutin;
extern crate nalgebra;

use self::glutin::VirtualKeyCode;
use self::nalgebra::Vector3;
use super::Camera;

pub struct InputTable {
    //
}

impl InputTable {
    pub fn new() -> Self {
        Self {}
    }

    pub fn update_keyboard(&self, camera: &mut Camera, keycode: VirtualKeyCode) {
        let (right_unit, up_unit, forward_unit) = camera.axis();
        let right = right_unit.unwrap();
        let up = up_unit.unwrap();
        let forward = forward_unit.unwrap();
        match keycode {
            VirtualKeyCode::Numpad6 => camera.move_position(-right),
            VirtualKeyCode::Numpad3 => camera.move_position(right),
            VirtualKeyCode::Numpad4 => camera.move_position(-up),
            VirtualKeyCode::Numpad1 => camera.move_position(up),
            VirtualKeyCode::Numpad5 => camera.move_position(-forward),
            VirtualKeyCode::Numpad8 => camera.move_position(forward),
            VirtualKeyCode::Numpad2 => camera.reset(),
            VirtualKeyCode::Numpad0 => camera.new_reset(),
            _ => {}
        }
    }

    pub fn update_mouse(&self, camera: &mut Camera, delta: (f64, f64)) {
        let (delta_x, delta_y) = delta;
        const SENSITIVITY: f64 = 1.0;
        let pitch = ((delta_x / SENSITIVITY) as f32).to_radians();
        let yaw = ((delta_y / SENSITIVITY) as f32).to_radians();
        camera.move_angle(Vector3::new(yaw, pitch, 0.0));
    }

    pub fn update_scroll(&self, camera: &mut Camera, delta: glutin::MouseScrollDelta) {
        const SENSITIVITY: f32 = 20.0;
        if let glutin::MouseScrollDelta::LineDelta(_, delta_y) = delta {
            camera.move_fov(-(delta_y / SENSITIVITY))
        }
    }
}
