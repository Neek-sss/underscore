extern crate obj;

pub trait Vertex {
    fn new(obj: &obj::Obj<obj::SimplePolygon>, indices: &obj::IndexTuple) -> Self;
}
