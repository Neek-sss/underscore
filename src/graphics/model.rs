extern crate gfx;
extern crate obj;

use super::{texture::load_texture, traits};
use std::{collections::HashMap, env, fs::File, io::BufReader, path::Path};

pub fn load_model<F, R, C, V>(
    obj_file_name: &str,
    asset_path: &Path,
    factory: &mut F,
    encoder: &mut gfx::Encoder<R, C>,
) -> (
    HashMap<String, gfx::handle::ShaderResourceView<R, [f32; 4]>>,
    HashMap<String, Vec<(String, gfx::handle::Buffer<R, V>, gfx::Slice<R>)>>,
)
where
    F: gfx::Factory<R> + gfx::traits::FactoryExt<R>,
    R: gfx::Resources,
    C: gfx::CommandBuffer<R>,
    V: traits::Vertex + gfx::traits::Pod + gfx::pso::buffer::Structure<gfx::format::Format>,
{
    let obj_file_path = asset_path.join(obj_file_name);
    let mut obj: obj::Obj<obj::SimplePolygon> = obj::Obj::load(&obj_file_path).expect(&format!(
        "Path: {}",
        obj_file_path.to_str().unwrap_or("Invalid Path Object")
    ));
    obj.material_libs.iter_mut().for_each(|mtl_lib_path| {
        *mtl_lib_path = env::current_dir()
            .expect("Error Getting CWD")
            .join(asset_path.to_str().expect("Invalid Path Object"))
            .join(mtl_lib_path.clone())
            .to_str()
            .expect("Invalid Path Object")
            .to_string();
        println!("{}", mtl_lib_path);
    });
    obj.load_mtls().expect("Error Loading MTLs");

    let mtls: Vec<_> = obj
        .material_libs
        .iter_mut()
        .map(|mtl_file_name| {
            let mtl_file_path = asset_path.join(mtl_file_name);
            let file = File::open(mtl_file_path.clone()).expect(&format!(
                "File Open Error; Path: {}",
                mtl_file_path.to_str().expect("Invalid Path Object")
            ));
            let mut reader = BufReader::new(file);
            obj::Mtl::load(&mut reader)
        }).collect();

    let textures: HashMap<String, _> = mtls.iter().fold(HashMap::new(), |mut acc, mtl| {
        mtl.materials.iter().for_each(|material| {
            let mtl_file_name = material.map_kd.clone().unwrap();
            let mtl_file_path = asset_path.join(mtl_file_name);
            let tex = load_texture(factory, &mtl_file_path);
            encoder.generate_mipmap(&tex);
            acc.insert(
                (*mtl_file_path
                    .to_str()
                    .expect("Invalid Path Object")
                    .clone())
                    .to_string(),
                tex,
            );
        });
        acc
    });
    let objects: HashMap<_, Vec<_>> = obj
        .objects
        .iter()
        .map(|object| {
            (
                object.name.clone(),
                object
                    .groups
                    .iter()
                    .map(|group| {
                        let vertices = group.polys.iter().fold(Vec::new(), |mut acc, poly| {
                            poly.iter()
                                .skip(1)
                                .collect::<Vec<_>>()
                                .as_slice()
                                .windows(2)
                                .for_each(|triangle| {
                                    acc.push(V::new(&obj, &poly[0]));
                                    acc.push(V::new(&obj, triangle[0]));
                                    acc.push(V::new(&obj, triangle[1]));
                                });
                            acc
                        });
                        let (vertex_buffer, slice) =
                            factory.create_vertex_buffer_with_slice(&vertices, ());
                        (
                            group.material.clone().unwrap().map_kd.clone().unwrap(),
                            vertex_buffer,
                            slice,
                        )
                    }).collect(),
            )
        }).collect();
    (textures, objects)
}

pub fn load_model_no_tex<F, R, C, V>(
    obj_file_name: &str,
    asset_path: &Path,
    factory: &mut F,
    encoder: &mut gfx::Encoder<R, C>,
) -> HashMap<String, Vec<(String, gfx::handle::Buffer<R, V>, gfx::Slice<R>)>>
where
    F: gfx::Factory<R> + gfx::traits::FactoryExt<R>,
    R: gfx::Resources,
    C: gfx::CommandBuffer<R>,
    V: traits::Vertex + gfx::traits::Pod + gfx::pso::buffer::Structure<gfx::format::Format>,
{
    let obj_file_path = asset_path.join(obj_file_name);
    let mut obj: obj::Obj<obj::SimplePolygon> = obj::Obj::load(&obj_file_path).expect(&format!(
        "Path: {}",
        obj_file_path.to_str().unwrap_or("Invalid Path Object")
    ));

    let objects: HashMap<_, Vec<_>> = obj
        .objects
        .iter()
        .map(|object| {
            (
                object.name.clone(),
                object
                    .groups
                    .iter()
                    .map(|group| {
                        let vertices = group.polys.iter().fold(Vec::new(), |mut acc, poly| {
                            poly.iter()
                                .skip(1)
                                .collect::<Vec<_>>()
                                .as_slice()
                                .windows(2)
                                .for_each(|triangle| {
                                    acc.push(V::new(&obj, &poly[0]));
                                    acc.push(V::new(&obj, triangle[0]));
                                    acc.push(V::new(&obj, triangle[1]));
                                });
                            acc
                        });
                        let (vertex_buffer, slice) =
                            factory.create_vertex_buffer_with_slice(&vertices, ());
                        (
                            group.material.clone().unwrap().map_kd.clone().unwrap(),
                            vertex_buffer,
                            slice,
                        )
                    }).collect(),
            )
        }).collect();
    objects
}
