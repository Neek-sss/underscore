#[macro_use]
extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;
extern crate image;
extern crate nalgebra;
extern crate obj;
extern crate rhai;
extern crate underscore;

mod graphics;

use self::underscore::{EngineExt, ModList};
use gfx::{traits::FactoryExt, Device};
use glutin::{GlContext, GlRequest};
use graphics::{model, Camera, InputTable};
use nalgebra::{Isometry3, Perspective3, Point3, Vector3};
use std::{collections::HashMap, path::Path};

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

const CLEAR_COLOR: [f32; 4] = [0.2, 0.2, 0.2, 1.0];

gfx_defines!{
    vertex Vertex {
        pos: [f32; 3] = "a_Pos",
        uv: [f32; 2] = "a_Uv",
        norm: [f32;3] = "a_Norm",
    }

    constant Uniforms {
        transform: [[f32; 4];4] = "u_Transform",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        tex: gfx::TextureSampler<[f32; 4]> = "t_Texture",
        uniforms: gfx::ConstantBuffer<Uniforms> = "Uniforms",
        out_color: gfx::RenderTarget<ColorFormat> = "Target0",
        out_depth: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
    }
}

impl underscore::Vertex for Vertex {
    fn new(obj: &obj::Obj<obj::SimplePolygon>, indices: &obj::IndexTuple) -> Self {
        Vertex {
            pos: obj.position[indices.0],
            uv: [
                obj.texture[indices.1.unwrap()][0],
                1.0 - obj.texture[indices.1.unwrap()][1], // OpenGL Y's are flipped
            ],
            norm: obj.normal[indices.2.unwrap()],
        }
    }
}

const HEIGHT: f64 = 600.0;
const WIDTH: f64 = 800.0;

pub fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let windowbuilder = glutin::WindowBuilder::new()
        .with_title("_5C0R3".to_string())
        .with_dimensions(glutin::dpi::LogicalSize::new(WIDTH, HEIGHT));
    let contextbuilder = glutin::ContextBuilder::new().with_gl(GlRequest::Latest);
    let (window, mut device, mut factory, color_view, depth_view) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(
            windowbuilder,
            contextbuilder,
            &events_loop,
        );

    let pso = factory
        .create_pipeline_simple(
            include_bytes!("graphics/shaders/shader.vert"),
            include_bytes!("graphics/shaders/shader.frag"),
            pipe::new(),
        ).unwrap();
    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();
    let sampler = factory.create_sampler_linear();

    let mut engine = rhai::Engine::new();
    engine.load_attribute_table();

    let mut textures = HashMap::new();
    let mut objects = HashMap::new();

    let mut mod_list = ModList::new();
    mod_list.load_mods::<_, _, _, Vertex>(
        &mut engine,
        &mut factory,
        &mut encoder,
        &mut textures,
        &mut objects,
    );

    let unit_type_name = "Cube";
    let unit_type = &mod_list.unit_types.clone().unwrap()[unit_type_name];
    let object = &objects
        .get(&unit_type.model.clone().unwrap())
        .expect(&format!("Could Not Find Model For \"{}\"", unit_type_name));
    let group_datas: Vec<_> = object
        .iter()
        .map(|group| {
            let uniforms_buffer = factory.create_constant_buffer(1);
            let texture = &textures
                .iter()
                .find(|tex| Path::new(tex.0).ends_with(group.0.clone()))
                .unwrap()
                .1
                .clone();
            (
                &group.2,
                pipe::Data {
                    vbuf: group.1.clone(),
                    uniforms: uniforms_buffer,
                    tex: (texture.clone(), sampler.clone()),
                    out_color: color_view.clone(),
                    out_depth: depth_view.clone(),
                },
            )
        }).collect();

    let input_table = InputTable::new();
    let mut camera = Camera::new()
        .with_position(Point3::new(0.0, 0.0, -3.0))
        .with_angle(Vector3::new(0.0, 0.0, 0.0));

    window
        .set_cursor_position(glutin::dpi::LogicalPosition::new(WIDTH / 2.0, HEIGHT / 2.0))
        .expect("Cursor Set Error");
    window.hide_cursor(true);

    let mut running = true;
    let mut focused = true;
    while running {
        events_loop.poll_events(|event| {
            if let glutin::Event::WindowEvent { event, .. } = event {
                match event {
                    glutin::WindowEvent::Focused(is_focused) => focused = is_focused,
                    glutin::WindowEvent::CloseRequested
                    | glutin::WindowEvent::KeyboardInput {
                        input:
                            glutin::KeyboardInput {
                                virtual_keycode: Some(glutin::VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => running = false,
                    glutin::WindowEvent::KeyboardInput {
                        input:
                            glutin::KeyboardInput {
                                virtual_keycode, ..
                            },
                        ..
                    } => {
                        if let Some(keycode) = virtual_keycode {
                            input_table.update_keyboard(&mut camera, keycode);
                        }
                    }
                    glutin::WindowEvent::MouseWheel { delta, .. } => {
                        input_table.update_scroll(&mut camera, delta);
                    }
                    _ => {}
                }
            } else if let glutin::Event::DeviceEvent { event, .. } = event {
                match event {
                    glutin::DeviceEvent::MouseMotion { delta, .. } => {
                        if focused {
                            input_table.update_mouse(&mut camera, delta);
                        }
                    }
                    _ => {}
                }
            }
        });

        if focused {
            window
                .set_cursor_position(glutin::dpi::LogicalPosition::new(WIDTH / 2.0, HEIGHT / 2.0))
                .expect("Cursor Set Error");
        }

        let transform = {
            let model = Isometry3::identity();
            let view = camera.view();
            let projection =
                Perspective3::new(WIDTH as f32 / HEIGHT as f32, camera.get_fov(), 0.1, 1000.0);
            let mvp = projection.as_matrix() * (view * model).to_homogeneous();
            [
                [mvp[0], mvp[1], mvp[2], mvp[3]],
                [mvp[4], mvp[5], mvp[6], mvp[7]],
                [mvp[8], mvp[9], mvp[10], mvp[11]],
                [mvp[12], mvp[13], mvp[14], mvp[15]],
            ]
        };

        encoder.clear(&color_view, CLEAR_COLOR);
        encoder.clear_depth(&depth_view, 1.0);
        for (slice, pipe_data) in &group_datas {
            encoder
                .update_buffer(
                    &pipe_data.uniforms,
                    &[Uniforms {
                        transform: transform,
                    }],
                    0,
                ).expect("Pipeline Encoding Error");
            encoder.draw(slice, &pso, pipe_data);
        }

        encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();
    }
}
